package lab.dynamic;

import lab.dev.TeamMember;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class SetterMain {

    public static void main(String[] args) throws IllegalAccessException {

        Class clazz = TeamMember.class;

        TeamMember tm = new TeamMember(null);

        // TODO 1: detect fields without setters
        // TODO 2: detect fields without setters and having no value
        // TODO 3: detect fields without setters and having no value and annotaded with @DefaultValue
        // setter: void method starting with 'set' then field name uppercase
        // with single param of type same like field type

        for(Field field : clazz.getDeclaredFields()){

            String setterMethodName = "set" + field.getName().substring(0,1).toUpperCase()
                    + field.getName().substring(1);
            Class setterParameterType = field.getType();

            boolean found = false;
            try {
                Method method = clazz.getDeclaredMethod(setterMethodName, setterParameterType);
                if (method.getReturnType() == void.class){
                    found = true;
                }
            }catch (NoSuchMethodException e){
                // ignore
            }
            if(!found){
                field.setAccessible(true);
                Object value = field.get(tm);
                if(value==null){
                    System.out.println("field " + field + " has no setter and value");
                    DefaultValue defaultValue = field.getAnnotation(DefaultValue.class);
                    if(defaultValue!=null){
                        System.out.println("default value annotation detected. value: " + defaultValue.value());
                        field.set(tm, defaultValue.value());
                    }
                }
            }

            System.out.println("setter for a field " + field.getName() + " found: " + found);
        }

        System.out.println("tm=" + tm);

    }
}
