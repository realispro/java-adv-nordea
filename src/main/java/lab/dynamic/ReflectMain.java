package lab.dynamic;

import lab.zoo.Animal;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Set;
import java.util.stream.Stream;

public class ReflectMain {

    public static void main(String[] args) {

        String className = "lab.zoo.animals.fish.Merlin";
        String name = "Nemo";
        int mass = -203;

        try {
            Class clazz = Class.forName(className);
            Constructor constructor = clazz.getDeclaredConstructor(String.class, int.class);
            Object o = constructor.newInstance(name, mass);

            if(o instanceof Animal a) {

                System.out.println("Animal created: " + a.getClass().getSimpleName()
                        + ", " + a.getName() + ", " + a.getMass());

                /*getFieldsStream(clazz)
                        .forEach(f->{
                            Positive positive = f.getAnnotation(Positive.class);
                            if(positive!=null){
                                System.out.println("annotation Positive found on a field " + f.getName());
                                try {
                                    f.setAccessible(true);
                                    Object value = f.get(a);
                                    if(value instanceof Integer){
                                        int intValue = (int) value;
                                        if(intValue<0 || intValue>positive.max()){
                                            throw new RuntimeException("constraint violation on a field " + f.getName());
                                        }
                                    }
                                } catch (IllegalAccessException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        });*/

                ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
                Validator validator = factory.getValidator();

                Set<ConstraintViolation<Animal>> violations = validator.validate(a);
                System.out.println("violations=" + violations);

                getFieldsStream(clazz)
                        .filter(f->f.getName().equals("mass"))
                        .forEach(f->{
                            try {

                                f.setAccessible(true);

                                f.set(a, 102);

                                Object value = f.get(a);
                                System.out.println("field " + f.getName() + " has value " + value);

                            } catch (IllegalAccessException e) {
                                throw new RuntimeException(e);
                            }
                        });

            } else {
                System.out.println("unexpected object type");
            }


        } catch (ClassNotFoundException | NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private static Stream<Field> getFieldsStream(Class clazz){

        Stream<Field> stream = Stream.empty();

        while(clazz!=null){
            stream = Stream.concat(stream, Stream.of(clazz.getDeclaredFields()));
            clazz = clazz.getSuperclass();
        }

        return stream;
    }


}
