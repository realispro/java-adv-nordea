package lab.dynamic;

import lab.valid.PercentageValidator;
import lab.zoo.animals.fish.Oyster;

public class ClassViewerMain {

    public static void main(String[] args) {

        Class clazz = getClazz();

        do {
            ClassViewer viewer = new ClassViewer(clazz);
            viewer.classInfo();
            viewer.fieldsInfo();
            viewer.constructorInfo();
            viewer.methodInfo();

            clazz = clazz.getSuperclass();
        }while (clazz!=Object.class);

    }


    public static Class getClazz(){
        return Oyster.class;
                // PercentageValidator.class;
                //Oyster.Pearl.class;
    }

}
