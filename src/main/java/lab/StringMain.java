package lab;

import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringMain {

    public static void main(String[] args) {


        String names = "Joe John Johnny";

        // splitting
        String[] namesArray = names.split(" ");
        System.out.println("namesArray = " + Arrays.toString(namesArray));

        StringTokenizer tokenizer = new StringTokenizer(names);

        while(tokenizer.hasMoreTokens()){

            String name = tokenizer.nextToken();
            if(name.equals("John")){
                System.out.println("John is present!");
                break;
            }
        }

        // joining
        StringBuilder builder = new StringBuilder();
        builder.append("[");

        for(String name : namesArray){
            builder.append(name).append(",");
        }
        
        builder.append("]");
        System.out.println("join = " + builder.toString());
        

        // regexp
        String patternText = "\\d{2}[-\\s]?\\d{3}";
        String text = "Warszawa 00-950, Krakow 30123, Torun 88 123";

        Pattern pattern = Pattern.compile(patternText);
        Matcher matcher = pattern.matcher(text);
        while(matcher.find()){
            System.out.println("pattern " + matcher.group() + " found at " + matcher.start() + "-" + matcher.end());
        }


    }
}
