package lab.box;

public class BoxMain {

    public static void main(String[] args) {
        System.out.println("Let's box!");

        Box<Integer> box = BoxFactory.create(102); //new IntegerBox(); //new NumberBox<>(); //new Box<>();
        //box.setContent(102);

        Integer content = box.getContent();

        System.out.println("content = " + content);


        System.out.println("done.");
    }
}
