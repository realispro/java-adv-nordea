package lab.box;

public interface BoxFactory {


    static <C> Box<C> create(C content){
        Box<C> box = new Box<>();
        box.setContent(content);
        return box;
    }


}
