package lab.valid;

@FunctionalInterface
public interface Validator<T> {

    boolean validate(T t);

    default String getDescription(){
        return prefix() + " Unknown";
    }

    static String foo(){
        return "Foo";
    }

    private String prefix(){
        return "prefix:";
    }

}
