package lab.valid;

public class PercentageValidator implements Validator<Integer>{
    @Override
    public boolean validate(Integer value) {
        return value>=0&&value<=100;
    }

    @Override
    public String getDescription() {
        return "percentage";
    }
}
