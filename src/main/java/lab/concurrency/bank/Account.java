package lab.concurrency.bank;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Account {

    private AtomicInteger amount;

    public Account(int amount) {
        this.amount = new AtomicInteger(amount);
    }

    public void deposit(int value){
        amount.addAndGet(value);
    }

    public void withdraw(int value){
        amount.addAndGet(-value);
    }

    public int getAmount() {
        return amount.get();
    }
}
