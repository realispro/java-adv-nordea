package lab;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class WhatsNewMain {

    public static void main(String[] args) {

        DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();
        String typeOfDay = "";
        switch (dayOfWeek) {
            case MONDAY:
            case TUESDAY:
            case WEDNESDAY:
            case THURSDAY:
            case FRIDAY:
                typeOfDay = "Working Day";
                break;
            case SATURDAY:
            case SUNDAY:
                typeOfDay = "Day Off";
        }
        System.out.println("typeOfDay = " + typeOfDay);

        String dateType = switch (dayOfWeek){
            case MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY -> "working";
            case SATURDAY, SUNDAY -> "weekend";
        };

        String prettyPrintJson = """
                {
                    "foo": "Foo",
                    "bar": %d,
                }
                """;

        System.out.printf(prettyPrintJson, 123);

        User iksinski = new User(123, "Iksinski");
        System.out.println("iksinski name: " + iksinski.name());


    }

    public record User(int id, String name){}

}
