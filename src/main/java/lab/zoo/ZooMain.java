package lab.zoo;

import lab.zoo.animals.bird.Eagle;
import lab.zoo.animals.bird.Kiwi;
import lab.zoo.animals.bird.Parrot;
import lab.zoo.animals.fish.Merlin;
import lab.zoo.animals.fish.Oyster;
import lab.zoo.animals.fish.Shark;
import lab.zoo.animals.fish.Tuna;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class ZooMain {

    public static void main(String[] args) {
        System.out.println("Let's visit zoo!");

        Eagle bielik = new Eagle("Bielik", 15);
        Eagle george = new Eagle("George", 13);
        Parrot ara = new Parrot("Ara", 3);
        Parrot ara2 = new Parrot("Ara", 3);
        Merlin nemo = new Merlin("Nemo", 199);
        Shark willy = new Shark("Willy", 299);
        Kiwi joe = new Kiwi("Joe", 5);
        Tuna jack = new Tuna("Jack", 50);

        List<Animal> animals = new LinkedList<>();
                //new ArrayList<>();
        //animals = Collections.unmodifiableList(animals);

        animals.add(bielik);
        animals.add(george);
        animals.add(ara);
        animals.add(0, ara2);
        animals.add(nemo);
        animals.add(willy);
        animals.add(joe);
        animals.add(jack);

        // filtering
        animals.removeIf(a -> a.getName().equalsIgnoreCase("nemo"));

        // sorting
        Collections.sort(animals, (a1, a2) -> a1.getMass()-a2.getMass());

        // reviewing
        Consumer<Animal> animalConsumer = System.out::println; //a -> System.out.println(a);
        animals.forEach(animalConsumer);

        Animal animal = animals.get(4);
        System.out.println("animals[4] = " + animal);
        animal.eat("oat");


        Oyster oyster = new Oyster("oyster", 1);
        Oyster.Pearl pearl = new Oyster.Pearl("sand");
        pearl.grow();
        pearl.grow();
        System.out.println("pearl mass:" + pearl.grow());


        System.out.println("done.");

    }
}
