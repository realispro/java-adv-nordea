package lab.zoo;

import lab.dynamic.Positive;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
@Slf4j
public abstract class Animal {

    private String name;
    @Positive(max=300)
    @javax.validation.constraints.Positive
    private int mass;

    public void eat(String food){
        log.info("{} is eating {}", name, food);
        //System.out.println(name + " is eating " + food);
    }

    public abstract void move();

}
