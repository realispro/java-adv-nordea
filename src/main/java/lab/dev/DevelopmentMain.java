package lab.dev;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Member;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.OptionalDouble;

@Slf4j
public class DevelopmentMain {

    public static void main(String[] args) {

        System.out.println("Let's develop!");
        Team team = constructTeam("A Team");
        List<TeamMember> members = team.getMembers();

        // TODO 1: sort by skills count
        log.info("\n\nTODO 1: sort by skills count");
        Comparator<TeamMember> comparator = (m1, m2) -> m1.getSkills().size() - m2.getSkills().size();
        members.sort(comparator.reversed());
        members.forEach(m -> System.out.println("m = " + m));

        // TODO 2: remove technical writer
        System.out.println("Removing technical writer from members list!");
        List<TeamMember> noWritter = new LinkedList<>(members);
        noWritter.removeIf(m -> m.getRoles().contains(Role.TECHNICAL_WRITER));
        System.out.println("members = " + noWritter);

        // TODO 3: find all java developers
        List<TeamMember> javaDevelopers = new LinkedList<>();
                /*members.stream()
                        .filter(m -> m.getRoles().contains(Role.DEVELOPER))
                        .filter(m -> m.getSkills().contains(Skill.JAVA))
                        .collect(Collectors.toList());*/
        members.forEach(m-> {
            if (m.getSkills().contains(Skill.JAVA) && m.getRoles().contains(Role.DEVELOPER)){
                javaDevelopers.add(m);
            }
        });
        System.out.println("Java Developers");
        System.out.println(javaDevelopers);

        // TODO 4: count all javascript developers
        long count = 0;
                /*members.stream()
                .filter(m -> m.getSkills().contains(Skill.JAVASCRIPT))
                .filter(m->m.getRoles().contains(Role.DEVELOPER))
                .count();*/
        for(TeamMember m : members){
            if(m.getSkills().contains(Skill.JAVASCRIPT)&&m.getRoles().contains(Role.DEVELOPER)){
                count = count + 1;
            }
        }

        System.out.println("count = " + count);

        // TODO 11: get all skills
        var distinctAllSkills = members.stream()
                .map(TeamMember::getSkills)
                .flatMap(set->set.stream())
                .collect(Collectors.toSet());
        System.out.println("distinctAllSkills = " + distinctAllSkills);

        // TODO 12: get all members except technical writer
        System.out.println("bez writera");
        Set<TeamMember> noWriterMembers = members.stream()
                .filter(m -> !m.getRoles().contains(Role.TECHNICAL_WRITER))
                        .collect(Collectors.toSet());
                    //.forEach(System.out::println);
        System.out.println("noWriterMembers = " + noWriterMembers);

        // TODO 13: get all developers names sorted by roles count
        var result13 = members.stream()
                .filter(m->m.getRoles().contains(Role.DEVELOPER))
                .sorted((m1,m2)->m1.getRoles().size()-m2.getRoles().size())
                .peek(a-> System.out.println("peek:" + a))
                .map(TeamMember::getName)
                .collect(Collectors.toList());
        System.out.println("result13 = " + result13);
        
        // TODO 14: get any java developer
        var toDo14 = members.stream()
                .filter(m -> m.getSkills().contains(Skill.JAVA))
                .filter(m->m.getRoles().contains(Role.DEVELOPER))
                .findAny();
        System.out.println("AnyJavaDev = " + toDo14.orElse(null));

        // TODO 15: calculate average skills count
        log.info("\n\n15: calculate average skills count");
        OptionalDouble averageSkillsCount = members.stream().mapToInt(m -> m.getSkills().size())
                .average();
        System.out.println("averageSkillsCount = " + averageSkillsCount);

        Integer averageSkillCount = members.stream()
                .map(m->m.getSkills().size())
                .reduce(0, (i1,i2)->i1+i2);
        System.out.println("averageSkillCount = " + ((double)averageSkillCount/members.size()));

        System.out.println("done.");
    }

    public static Team constructTeam(String name) {
        Team team = new Team(name);

        TeamMember joe = new TeamMember("Joe")
                .withSkill(Skill.JAVA)
                .withSkill(Skill.JPA)
                .withSkill(Skill.SQL)
                .withSkill(Skill.SPRING)
                .withRole(Role.DEVELOPER);

        TeamMember joeBis = new TeamMember("Joe")
                .withSkill(Skill.JAVA)
                .withSkill(Skill.JPA)
                .withSkill(Skill.SQL)
                .withSkill(Skill.SPRING)
                .withRole(Role.DEVELOPER);

        team.addMember(joeBis);

        team.addMember(new TeamMember("Jane")
                .withSkill(Skill.ANGULAR)
                .withSkill(Skill.JAVASCRIPT)
                .withSkill(Skill.JAVA)
                .withSkill(Skill.PYTHON)
                .withSkill(Skill.SPRING)
                .withRole(Role.DEVELOPER)
                .withRole(Role.SCRUM_MASTER));

        team.addMember(new TeamMember("Bob")
                .withSkill(Skill.JAVASCRIPT)
                .withSkill(Skill.REACT_JS)
                .withSkill(Skill.PYTHON)
                .withSkill(Skill.SQL)
                .withSkill(Skill.SPRING)
                .withRole(Role.QA));

        team.addMember(new TeamMember("Betty")
                .withSkill(Skill.PYTHON)
                .withSkill(Skill.SQL)
                .withRole(Role.QA));

        team.addMember(joe);

        team.addMember(new TeamMember("Billy")
                .withSkill(Skill.SQL)
                .withRole(Role.TECHNICAL_WRITER)
                .withRole(Role.PRODUCT_OWNER));

        return team;
    }
}
